def get_lines(fname):
    path = pathlib.Path(__file__).parent / fname
    
    with path.open("rb") as _myfile:
        lines = _myfile.readlines()

    for idx, _line in enumerate(lines):
        lines[idx] = (_line
            .decode('utf-8-sig', 'ignore')
            .strip()
        )
    
    return lines


def colsort(lines, valnames):
    cols = {}
    
    for _valname in valnames: 
        cols[_valname] = []

    for _line in lines:
        vals = _line.split(";")

        if not len(vals) == len(valnames):
            raise Exception(f"Size of values {len(vals)} does not match size of value names {len(valnames)}")

        idx = 0

        for val in vals:
            cols[valnames[idx]].append(val)
            idx += 1

            if idx >= len(valnames):
                idx = 0
        
    return cols


def split_io(pins, input_names, output_names):
    inputs = {}
    outputs = {}

    for pin in pins:
        for _input in input_names:
            if pin == _input:
                inputs[_input] = pins[pin]
        
        for _output in output_names:
            if pin == _output:
                outputs[_output] = pins[pin]
    
    return (inputs, outputs)


def lines_to_io(lines, input_names, output_names):
    pins = colsort(lines, input_names + output_names)
    (inputs, outputs) = split_io(pins, input_names, output_names)
    return (inputs, outputs)


def io_to_eq(inputs, outputs, lines, input_names):
    equations = {}

    for output_letter in outputs:
        equations[output_letter] = ""
        equation = ""

        for idx, val in enumerate(outputs[output_letter]):
            if int(val) == 1:
                for n, _inp_val in enumerate(lines[idx].split(";")[0 : len(input_names) : 1]): # Shady I know...
                    if int(_inp_val) == 0:
                        equation += input_names[n] + "'"
                    
                    else:
                        equation += input_names[n]

                    equation += "*"
                
                equation = equation[:-1]
                equation += "+"

        equation = equation[:-1]
        equations[output_letter] = equation

    return equations


def apply_basic_rules(piece):
    # Some black magic math..
    return piece


def shorten(equations, output_names):
    short_set = {}

    for equation in equations:
        eq_str = equations[equation]
        parts = eq_str.split("+")
        parts = list(dict.fromkeys(parts))

        curr_index = 0
        can_short = True
        short_str = parts[curr_index]

        while can_short:
            attempt = apply_basic_rules(short_str)

            if attempt == short_str:
                print("yes")
                if not curr_index + 1 == len(parts):
                    short_str += parts[curr_index + 1]
                
                else:
                    can_short = False
        
        short_set[equation] = short_str
    
    return short_set
        

def pretty_print(equations):
    for expression in equations:
        print(f"{expression} = {equations[expression]}\n")
        

def main(args):
    lines = get_lines(args.fname)

    (inputs, outputs) = lines_to_io (
        lines, args.inputs, args.outputs
    )

    equations = io_to_eq(inputs, outputs, lines, args.inputs)
    #equations = shorten(equations, args.outputs)
    pretty_print(equations)
    

if __name__ == "__main__":
    import pathlib
    import argparse
    import codecs
    import sys

    sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

    parser = argparse.ArgumentParser()

    parser.add_argument("--fname",
        help="csv file containing truth table", dest="fname", required=True)
    
    parser.add_argument("--inputs", nargs='+',
        help="space separated letters for the inputs", dest="inputs", required=True)
    
    parser.add_argument("--outputs", nargs='+',
        help="csv file containing truth table", dest="outputs", required=True)
    
    args = parser.parse_args()

    main(args)

    
